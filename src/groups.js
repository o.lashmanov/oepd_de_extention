var Global_sting = ""

$(".i_r_body>table").last().append('<div class="extention"></div>');
$(".extention").append('<textarea class="query">B3110, B3120</textarea>')
               .append('<textarea class="response" style="width:400"></textarea>')
               .append('<button class="i_button i_button_cold ext_query_btn" type="button"><span>Get List</span></button>');

var sendRequest = function(extraData) {
    return function(data, textStatus, jqXHR) {
        var rows = $(data).find("table.i_standard").find('tr') 
        rows = rows.slice(5,rows.size())
        var s = ""
        for (var i = 0; i < rows.size(); i++) {
            if ($(rows[i]).find("td.i_col_3").text().length > 0)
            {
                s += extraData.group + ","
                    + $(rows[i]).find("td.i_col_3").text() + "," 
                    + $(rows[i]).find("td.i_col_4").text() + ","
                    + $(rows[i]).find("td.i_col_5").text() + ","
                    + $(rows[i]).find("td.i_col_6").text() + "\n";
            }
        }
        Global_sting += s;
        // console.log(s)
    }
};

$('.ext_query_btn').click(function() {
    var groups = Papa.parse($('.query').val());
    for (var i = groups.data[0].length - 1; i >= 0; i--) {
        groups.data[0][i] = groups.data[0][i].trim()
    }
    var target_g = groups.data[0]
    // TODO: validate groups
    Global_sting = ""
    for (var i = target_g.length - 1; i >= 0; i--) {
        var url = "https://isu.ifmo.ru/pls/apex/f?p=2005:6:102042701889474::NO::GROUP_GROUP:" + target_g[i]
        console.log("get", url)
        var extraData = {
            group: target_g[i]
        };
        $.ajax({
          url: url,
          success:  sendRequest(extraData),
          async: false
        })
    }
    $('.response').val(Global_sting)
    // console.log(Global_sting)
});

