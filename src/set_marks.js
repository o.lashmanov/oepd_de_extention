$('div:contains("Виды текущего контроля")').append('<button class = "b_ext" id="'+rndMark(60,67)+'">3 E</button>')
											.append('<button class = "b_ext" id="'+rndMark(67,74)+'">3 D</button>')
											.append('<button class = "b_ext" id="'+rndMark(74,83)+'">4 C</button>')
											.append('<button class = "b_ext" id="'+rndMark(83,91)+'">4 B</button>')
											.append('<button class = "b_ext" id="'+rndMark(91,100)+'">5 A</button>')
											.append('<input class = "custom_mark">')
											.append('<button class = "b_ext" id="0">custom</button>');


$('#navigationBack').parent().append('<table></table>').addClass('group_marks');
$('select[name = "PERSON"] option').each(function(){
	$('.group_marks').append('<tr class = "mass_person"><td class = "mass_person_name">'
							+ $(this).text()
							+ '</td><td class = "mass_person_id">'
							+ $(this).val()
							+ '</td><td><input class = "mass_person_mark"></td><td>0%</td></tr>'
							);
});
$('.group_marks').append('<tr><td></td></tr>')
				.append('<button class = "massMark_btn">massMark</button>')
				.append('<button class = "massMark_btn_1mod">1mod</button>')
				.append('<button class = "massMark_btn_2mod">2mod</button>');

$('.custom_mark').change(function() {
	var mark = $(this).val();
	if (mark > 100) {
		console.log('invalid mark: ' + mark);
		$(this).val(100);
	}else{
		console.log('custom mark: ' + mark);
		$(this).next().attr('id', mark);
	}
});

var dom_tr = $('#CurrentExams').next().next().children('tr').has('input:hidden').has('td :hidden');
var dom_tr_module_main1 = dom_tr.splice(0, 1);
dom_tr_module_main1 = $(dom_tr_module_main1);

var dom_tr_module1 = [];
var dom_tr_module2 = [];
var dom_tr_module_all = [];
var dom_tr_module_main2;

find_modules();


$('.b_ext').click(function() {

	for (var i = dom_tr_module_all.length - 1; i >= 0; i--) {
		$(dom_tr_module_all[i]).children(':nth-child(11)').children(':nth-child(2):enabled').css({'background-color' : '#fff'});
	};
	//dom_tr_module_all.children(':nth-child(11)').css({'background-color' : 'green'});

	var intExams = ''; 
	var personid = 		$('input[name = "PERSONID"]').val();
	var programid = 	$('input[name = "PROGRAMID"]').val();
	var semid = 		$('input[name = "SEMID"]').val();
	var st_grp = 		$('input[name = "ST_GRP"]').val();
	var apprenticeship =$('input[name = "APPRENTICESHIP"]').val();
	var scriptid = 		$('input[name = "SCRIPTID"]').val();
	var score = this.id;

	for (var i = dom_tr_module_all.length - 1; i >= 0; i--) {
		var el = $(dom_tr_module_all[i]).children(':nth-child(11)').children(':nth-child(2)');
		if (el.is(':enabled')) {
			el.val($(dom_tr_module_all[i]).children(':nth-child(5)').text()*score/100); 
			startcheck = true; 
			onchangeed = el.attr('id');
			var exam = el.attr('id') + '=' + el.val() + '|';
			sendAJAX(intExams,personid,programid,semid,st_grp,apprenticeship,scriptid,exam,succes_oneq,dom_tr_module_all.length)
		};
	};
});

$('.massMark_btn').click(function(){
	$('.mass_person_id').next().next().text('0%').css({'background-color' : '#fff'});
	$('.mass_person').each(function(){
		var name = $(this).children('.mass_person_name').text();
		var score = $(this).children().children('.mass_person_mark').val();
		if ($(this).children().children('.mass_person_mark').val() == '') {
			//console.log($(this).children('.mass_person_id').val());
		}else{
			var intExams = ''; 
			var personid = 		$(this).children('.mass_person_id').text();
			var programid = 	$('input[name = "PROGRAMID"]').val();
			var semid = 		$('input[name = "SEMID"]').val();
			var st_grp = 		$('input[name = "ST_GRP"]').val();
			var apprenticeship =$('input[name = "APPRENTICESHIP"]').val();
			var scriptid = 		$('input[name = "SCRIPTID"]').val();
			
			for (var i = dom_tr_module_all.length - 1; i >= 0; i--) {
				var el = $(dom_tr_module_all[i]).children(':nth-child(11)').children(':nth-child(2)');
				if (el.is(':enabled')) {
					el.val($(dom_tr_module_all[i]).children(':nth-child(5)').text()*score/100); 
					startcheck = true; 
					onchangeed = el.attr('id');
					var exam = el.attr('id') + '=' + el.val() + '|';
					sendAJAX(intExams,personid,programid,semid,st_grp,apprenticeship,scriptid,exam,succes_mass,dom_tr_module_all.length)
				};
			};
		};
		
	});

});

$('.massMark_btn_1mod').click(function(){
	$('.mass_person_id').next().next().text('0%').css({'background-color' : '#fff'});
	$('.mass_person').each(function(){
		var name = $(this).children('.mass_person_name').text();
		var score = $(this).children().children('.mass_person_mark').val()/dom_tr_module_main1.children(':nth-child(5)').text();

		if ($(this).children().children('.mass_person_mark').val() == '') {
			//console.log($(this).children('.mass_person_id').val());
		}else{
			var intExams = ''; 
			var personid = 		$(this).children('.mass_person_id').text();
			var programid = 	$('input[name = "PROGRAMID"]').val();
			var semid = 		$('input[name = "SEMID"]').val();
			var st_grp = 		$('input[name = "ST_GRP"]').val();
			var apprenticeship =$('input[name = "APPRENTICESHIP"]').val();
			var scriptid = 		$('input[name = "SCRIPTID"]').val();
			
			for (var i = dom_tr_module1.length - 1; i >= 0; i--) {
				var el = $(dom_tr_module1[i]).children(':nth-child(11)').children(':nth-child(2)');
				if (el.is(':enabled')) {
					//el.val(($(dom_tr_module1[i]).children(':nth-child(5)').text()*score).toFixed(2)); 
					startcheck = true; 
					onchangeed = el.attr('id');
					var exam = el.attr('id') + '=' + ($(dom_tr_module1[i]).children(':nth-child(5)').text()*score).toFixed(2) + '|';
					sendAJAX(intExams,personid,programid,semid,st_grp,apprenticeship,scriptid,exam,succes_mass,dom_tr_module1.length)
				};
			};
		};
		
	});

});

$('.massMark_btn_2mod').click(function(){
	$('.mass_person_id').next().next().text('0%').css({'background-color' : '#fff'});
	$('.mass_person').each(function(){
		var name = $(this).children('.mass_person_name').text();
		var score = $(this).children().children('.mass_person_mark').val()/dom_tr_module_main2.children(':nth-child(5)').text();
		if ($(this).children().children('.mass_person_mark').val() == '') {
			//console.log($(this).children('.mass_person_id').val());
		}else{
			var intExams = ''; 
			var personid = 		$(this).children('.mass_person_id').text();
			var programid = 	$('input[name = "PROGRAMID"]').val();
			var semid = 		$('input[name = "SEMID"]').val();
			var st_grp = 		$('input[name = "ST_GRP"]').val();
			var apprenticeship =$('input[name = "APPRENTICESHIP"]').val();
			var scriptid = 		$('input[name = "SCRIPTID"]').val();
			
			for (var i = dom_tr_module2.length - 1; i >= 0; i--) {
				var el = $(dom_tr_module2[i]).children(':nth-child(11)').children(':nth-child(2)');
				if (el.is(':enabled')) {
					//el.val(($(dom_tr_module2[i]).children(':nth-child(5)').text()*score).toFixed(2)); 
					startcheck = true; 
					onchangeed = el.attr('id');
					var exam = el.attr('id') + '=' + ($(dom_tr_module2[i]).children(':nth-child(5)').text()*score).toFixed(2) + '|';
					sendAJAX(intExams,personid,programid,semid,st_grp,apprenticeship,scriptid,exam,succes_mass,dom_tr_module2.length)
				};
			};
		};
		
	});

});

function rndMark(min,max){
	return Math.ceil(min + Math.random()*(max-min))
}

function sendAJAX(intExams,personid,programid,semid,st_grp,apprenticeship,scriptid,exam,fun_success,num_of_e){
	var strUrl = 'distributedCDE?Rule=setExams' +
			                '&AJAX_UPDATE=1' +
			                '&INTERMEDIATEEXAMS=' + intExams +
			                '&CURRENTEXAMS=' + exam +
			                '&PERSONID='+personid+'&UNIVER=1&PROGRAMID='+programid+'&SEMID='+semid+'&ST_GRP='+st_grp+'&STATEID=-1&VARIABLEID=' +
			                '&INTERID=' +
			                '&APPRENTICESHIP='+apprenticeship+'&SCRIPTID='+scriptid+'&RNDPARAM=' + Math.round( Math.random()*1000 );
	console.log(strUrl);
	$.ajax({
			url: strUrl,
			success: function(data, textStatus, jqXHR){
				if (data == '1') {
					fun_success(this,data,num_of_e);
				} else{
					console.log('retry');
					sendAJAX(intExams,personid,programid,semid,st_grp,apprenticeship,scriptid,exam,fun_success,num_of_e)
				};
			}
	});
};

function succes_oneq(ob,data,num_of_e){
	var reg = new RegExp("CURRENTEXAMS=([0-9]*)");
	var id = reg.exec(ob.url)[1];
	$('#'+id).css({'background-color' : '#33CC33'});
 	console.log(id + ' done, code:' + data); 
 	$('input[name = "buttonSubmit3"]').removeAttr('disabled');
 	$('input[name = "buttonSubmit3"]').css({'color' : 'black'});
}

function succes_mass(ob,data,num_of_e){
	var reg = new RegExp("PERSONID=([0-9]*)");
	var personid = reg.exec(ob.url)[1];
	var p = $('.mass_person_id:contains('+personid+')').next().next();
	//var num_of_e = $('#CurrentExams').next().next().children('tr').has('input:hidden').children(':nth-child(11)').children(':nth-child(2):enabled').length;
	var a = ((p.text().substring(0,p.text().length -1))*1 + 100/num_of_e).toFixed(3);
	p.text( a + '%');
	if (a >99.9) {
		p.text('100%');
		p.css({'background-color' : '#33CC33'});
	}
 	console.log(personid + ' done, persent:' + a + '%, code: ' + data);
}

function find_modules(){
	var flag = false;
	dom_tr.each(function(){
		if($(this).has('td :contains(Модуль)').length > 0){
			dom_tr_module_main2 = $(this);
			flag = true;
		}else{
			if (!flag) {
				dom_tr_module1.push(this);
			} else{
				dom_tr_module2.push(this);
			};
			dom_tr_module_all.push(this);
		}
	});
}