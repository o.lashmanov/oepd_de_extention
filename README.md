# Описание
Репозиторий разработки плагина для google chrome, позволяющий автоматищировать рутинные операции на портале de.ifmo.ru 

# Установка
## Первый вариант (внимание не всегда современная версия)
Установка происходит путем простого перетаскивания файла в браузер файла ./build/de_ext.crx.

## Второй вариант (более предпочтительный)
Клонируем себе репозиторий 

``
cd path/than/oyu_love
git clone https://gitlab.com/o.lashmanov/oepd_de_extention.git
``

Идем в настройки google chrom > расширения > включить галку "Режим разработчика" > "Загрузить распакованное расширение..." > указать путь до `path/than/oyu_love/src`

# Работа
## Режим 1
После установки на странице простановки баллов студенту появляется ряд дополнительных элементов управления. (электронный журнал -> выбор группы, студента, дисциплины-> перейти к журналу).

Кнопки 3E, 3D, 4C, 4B и 5А автоматически проставляют студенту нужные баллы. Как только все поля "Рейтинг" станут зелеными оценки проставлены.
При необходимости проставить точную оценку ее можно ввести в текстовое поле и нажать кнопку "custom".

Кроме того, внизу страницы есть полный список группы и 3 кнопки. Напротив каждого студента есть текстовое поле куда заносятся баллы.
После того как все баллы проставлены нужно нажать одну из кнопок "massMark", "1mod", "2mod".

* "massMark" - выставить указанное количество баллов за весь семестр
* "1mod" - выставить указное количество баллов за первый модуль в семестре
* "2mod" - выставить указное количество баллов за второй модуль в семестре

После нажатия кнопки дождаться когда процент выполнения дойдет до 100% у всех студентов.

Если в текстовом поле напротив студента ничего не указано, то баллы не выставляются.

## Режим 2
После установки на странице списка групп будет доступно 2 поля внузу страницы в левое через запятую вносим номера груп, и нажимаем кнопку "Get List", ждем...
В правом поле можно копировать данные в формате csv

`Внимание: На время отправки запросов страница становится неозывчивой! Это связано с особенностями работы ISU`